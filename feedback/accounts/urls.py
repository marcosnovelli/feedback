from django.urls import path

from . import views

app_name = 'accounts'

urlpatterns = [
    path('login/', views.UserLogin.as_view(), name='user_login'),
    path('profile/', views.create_profile, name='create_profile'),
    path('dashboard/', views.dashboard, name='dashboard'),
    path('', views.UserCreateView.as_view(), name='add_user'),
]