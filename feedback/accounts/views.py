from django.contrib import messages
from django.shortcuts import render, redirect
from django.views.generic.edit import CreateView
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.utils.translation import ugettext as _
from django.contrib.auth.views import LoginView
from django.contrib.auth import authenticate, login

from .forms import ProfileForm

# Create your views here.

class UserCreateView(CreateView):
    model = User
    template_name = 'accounts/add_user.html'
    fields = [
        'first_name',
        'last_name',
        'username',
        'email',
        'password'
    ]
    success_url = reverse_lazy('accounts:add_user')
    
    def form_valid(self, form):
        f = form.save(commit=False)
        f.set_password(f.password)
        f.save()
        messages.success(self.request, _('Cadastro criado com sucesso!'))
        return redirect(self.success_url)


class UserLogin(LoginView):
    template_name= 'accounts/user_login.html'

    def post(self, request, *args, **Kwargs):
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            messages.success(request, _('Login efetuado com Sucesso.'))
            return redirect('accounts:user_login')
        messages.error(request, _('Erro, login não realizado.'))
        return redirect('accounts:user_login')

def dashboard(request):
    template_name = 'accounts/dashboard.html'
    return render(request, template_name, {})


def create_profile(request):
    template_name = 'accounts/profile.html'
    if request.method == 'POST':
        form = ProfileForm(request.POST, request.FILES)
        if form.is_valid():
            f = form.save(commit=False)
            f.user = request.user
            f.save()
            messages.success(request, _('Perfil gravado com sucesso!'))
            return redirect('accounts:dashboard')

    form = ProfileForm()
    context = {
        'form': form
    }
    return render(request, template_name, context)

