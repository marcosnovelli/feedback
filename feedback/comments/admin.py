from django.contrib import admin

from .models import Circle, Comments

# Register your models here.

admin.site.register(Circle)
admin.site.register(Comments)
