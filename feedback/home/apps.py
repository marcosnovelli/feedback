from tabnanny import verbose
from django.apps import AppConfig


class HomeConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'feedback.home'
    verbose_name = 'Home'
